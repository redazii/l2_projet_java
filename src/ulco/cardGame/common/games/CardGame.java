package ulco.cardGame.common.games;

import ulco.cardGame.common.games.boards.CardBoard;
import ulco.cardGame.common.games.components.Card;

import ulco.cardGame.common.games.components.Component;
import ulco.cardGame.common.interfaces.Player;

import java.io.*;
import java.net.Socket;
import java.util.*;

public class CardGame extends BoardGame {
    private List<Card> cards;
    private Integer numberOfRounds = 1;

    /**
     * Constructeur
     * @param name
     * @param maxPlayers
     * @param filename
     */
    public CardGame(String name, Integer maxPlayers, String filename) {
        super(name, maxPlayers, filename);
        this.board=new CardBoard();
    }

    /**
     * Initialisation du jeu de carte
     * @param filename
     */
    public void initialize(String filename) {
        this.cards = new ArrayList<>();

        try {
            File cardFile = new File(filename);
            Scanner myReader = new Scanner(cardFile);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                String split[] = data.split(";");
                cards.add(new Card(split[0], Integer.parseInt(split[1]),true));

            }
            myReader.close();

        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    public Player run(Map<Player, Socket> socketMapap) throws IOException, ClassNotFoundException {

        Map<Player, Card> playedCard = new HashMap<>();
        Player winner = null;

        int compteur = 0;
        Collections.shuffle(this.cards);
        for (Card carte : this.cards) {
            carte.setHidden(false);
            players.get(compteur % players.size()).addComponent(carte);
            compteur++;
        }

        for (Player joueur : this.players) {

            if (joueur.getScore() > 0) {

                joueur.canPlay(true);
            }
        }
        for (Map.Entry<Player, Socket> entry : socketMapap.entrySet()) {
            Socket socket = entry.getValue();
            StringBuilder statut = new StringBuilder();
            for (Player joueur : players) {
                if (joueur.isPlaying()) {
                    statut.append(joueur.getName()).append(" : ").append(joueur.getScore()).append("\n");
                }
            }
            statut.append("------------------------\n");
            ObjectOutputStream toutEstOkOos = new ObjectOutputStream(socket.getOutputStream());
            toutEstOkOos.writeObject(statut);
        }

        while (!end()) {

            Integer val = 0;

            if (this.numberOfRounds % 10 == 0) {
                for(Map.Entry<Player,Socket> enAttente : socketMapap.entrySet()){
                    Socket waitingSocket = enAttente.getValue();
                    ObjectOutputStream sortie = new ObjectOutputStream(waitingSocket.getOutputStream());
                    sortie.writeObject("Mélange...");
                }

                for (Player joueur : this.players) {

                    joueur.shuffleHand();
                }
            }

            for (Map.Entry<Player,Socket> joueurs : socketMapap.entrySet()) {
                Player joueur = players.get(players.indexOf(joueurs.getKey()));
                Socket socket = joueurs.getValue();
                if (joueur.isPlaying()) {
                    ObjectOutputStream sortie = new ObjectOutputStream(socket.getOutputStream());
                    sortie.writeObject(this);

                    for(Map.Entry<Player,Socket> enAttente : socketMapap.entrySet()){
                        String msg = "";
                        Player waitingPlayer = enAttente.getKey();
                        Socket waitingSocket = enAttente.getValue();

                        if(waitingPlayer != joueur){
                            msg += "En attente de "+joueur.getName()+" pour jouer";
                        }else{
                            msg += "["+joueur.getName()+"] you have to play ...";
                        }
                        ObjectOutputStream sortieMsg = new ObjectOutputStream(waitingSocket.getOutputStream());
                        sortieMsg.writeObject(msg);
                    }
                    ObjectInputStream carte = new ObjectInputStream(socket.getInputStream());
                    Card card = (Card) carte.readObject();
                    for(Component cartes : joueur.getSpecificComponents(Card.class)){
                        if(card.getValue().equals(cartes.getValue())){
                            card=(Card) cartes;
                            break;
                        }
                    }
                    joueur.removeComponent(card);

                    if(card.getValue()>val){
                        val=card.getValue();
                        winner = joueur;
                    }else if(card.getValue().equals(val) && Math.random()>0.5){
                        val=card.getValue();
                        winner=joueur;
                    }
                    playedCard.put(joueur,card);
                    for(Map.Entry<Player,Socket> enAttente : socketMapap.entrySet()){
                        Socket waitingSocket = enAttente.getValue();
                        String msg = joueur.getName()+ " a joué "+card.getName();
                        ObjectOutputStream cardPlayed = new ObjectOutputStream(waitingSocket.getOutputStream());
                        cardPlayed.writeObject(msg);
                    }
                }
            }

            for(Map.Entry<Player,Socket> enAttente : socketMapap.entrySet()){
                Socket waitingSocket = enAttente.getValue();
                ObjectOutputStream sortie1 = new ObjectOutputStream(waitingSocket.getOutputStream());
                sortie1.writeObject(this);
                ObjectOutputStream sortie2 = new ObjectOutputStream(waitingSocket.getOutputStream());
                sortie2.writeObject(board);
                String msg = winner.getName() + " a gagné ce round.";
                ObjectOutputStream sortie3 = new ObjectOutputStream(waitingSocket.getOutputStream());
                sortie3.writeObject(msg);
            }


            for (Map.Entry<Player, Card> enAttente : playedCard.entrySet()) {
                Card carte = enAttente.getValue();
                Player lwinner = enAttente.getKey();
                winner.addComponent(carte);
                if (lwinner.getScore() == 0) {
                    lwinner.canPlay(false);
                    for (Map.Entry<Player, Socket> still : socketMapap.entrySet()) {
                        Player lost = still.getKey();
                        Socket socket = still.getValue();
                        ObjectOutputStream sortie = new ObjectOutputStream(socket.getOutputStream());
                        if (lwinner != lost) {
                            sortie.writeObject(lwinner.getName() + " lost !!");
                        } else {
                            sortie.writeObject("You have lost !");
                        }
                    }
                }
            }

            playedCard.clear();
            board.clear();
            for (Map.Entry<Player, Socket> enAttente : socketMapap.entrySet()) {
                Socket waitingSocket = enAttente.getValue();
                ObjectOutputStream sortie = new ObjectOutputStream(waitingSocket.getOutputStream());
                StringBuilder score = new StringBuilder();
                for (Player joueur : players) {
                    if (joueur.isPlaying()) {
                        score.append(joueur.getName()).append(" : ").append(joueur.getScore()).append("\n");
                    }
                }
                sortie.writeObject("Round: " + numberOfRounds +
                        " ; Score: \n" + score.toString() +
                        "\n------------------------\n");

            }

            numberOfRounds++;
        }

        return winner;

    }



    /**
     * méthode fin du jeu
     * @return bool
     */
    public boolean end() {
        // check if it's the end of the game
        endGame = true;
        for (Player player : players) {
            if (player.isPlaying()) {
                endGame = false;
            }
        }

        return endGame;
    }


    /**
     *
     * @return name
     */
    public String toString() {
        return this.name;

    }

}
