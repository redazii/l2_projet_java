package ulco.cardGame.common.interfaces;

import ulco.cardGame.common.games.components.Component;

import java.io.IOException;
import java.io.Serializable;
import java.net.Socket;
import java.util.List;

public interface Player extends Serializable {


    String getName();


    Integer getScore();


    void play(Socket socket) throws IOException;


    boolean isPlaying();


    void canPlay(boolean playing);


    void addComponent(Component component);


    void removeComponent(Component component);


    List<Component> getComponents();

    List<Component> getSpecificComponents(Class classType);


    void shuffleHand();


    void clearHand();


    void displayHand();
}
