package ulco.cardGame.server;

import ulco.cardGame.common.games.CardGame;
import ulco.cardGame.common.games.PokerGame;
import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.interfaces.Game;
import ulco.cardGame.common.interfaces.Player;
import ulco.cardGame.common.players.CardPlayer;
import ulco.cardGame.common.players.PokerPlayer;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Constructor;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Server Game management with use of Singleton instance creation
 */
public class SocketServer {

    private ServerSocket server;
    protected Game game; // game will be instantiate later
    protected Map<Player, Socket> playerSockets;
    protected Constructor playerConstructor;

    public static final int PORT = 7778;

    private SocketServer() {
        try {
            server = new ServerSocket(PORT);
            playerSockets = new HashMap<>();
            playerConstructor = null;

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        SocketServer server = new SocketServer();
        server.run();
    }

    private void run() {

        //choix du jeu

        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("Which game do you want to play ? (Battle or Poker)");
            String gameName=scanner.nextLine();
            gameName=gameName.toLowerCase();

            if (gameName.equals("battle")) {
                game = new CardGame("Battle", 3, "resources/games/cardGame.txt");
                break;
            }

            if (gameName.equals("poker")) {
                game = new PokerGame("Poker", 3, "resources/games/pokerGame.txt", 2);
                break;
            }
        }

        System.out.println("Waiting for players to join " + game.toString());

        try {
            // add each player until game is ready (or number of max expected player is reached)
            // Waiting for the socket entrance
            do {

                //System.out.println("Waiting for new player to choose a game");
                Socket socket = server.accept();

                // read socket Data
                ObjectInputStream ois1 = new ObjectInputStream(socket.getInputStream());
                String playerUsername = (String) ois1.readObject();

                // Create player instance
                Player player=null;
                if (game instanceof PokerGame) {
                    player = new PokerPlayer(playerUsername);
                } else {
                    player = new CardPlayer(playerUsername);
                }


                // Add player in game
                if (game.addPlayer(socket,player)) {

                    ObjectOutputStream oos2 = new ObjectOutputStream(socket.getOutputStream());
                    oos2.writeObject(game);

                    // Tell to other players that new player is connected
                    for (Socket playerSocket : playerSockets.values()) {

                        ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                        playerOos.writeObject("Now connected: " + player.getName());
                    }

                    // Store player's socket in dictionary (Map)
                    playerSockets.put(player, socket);

                    System.out.println(player.getName() + " is now connected...");
                }


            } while (!game.isStarted());

            // run the whole game using sockets
            Player gameWinner = game.run(playerSockets);

            //affichage du gagnant aux joueurs
            for (Socket playerSocket : playerSockets.values()) {
                ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                playerOos.writeObject(gameWinner.getName() + " has winner !!!");
            }

            // Tells to player that server will be closed (just as example)
            for (Socket playerSocket : playerSockets.values()) {
                ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                playerOos.writeObject("END");
            }

            // Close each socket when game is finished
            for (Socket socket : playerSockets.values()) {
                socket.close();
            }

            game.removePlayers();

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}

